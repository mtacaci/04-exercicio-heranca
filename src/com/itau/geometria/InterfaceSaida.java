package com.itau.geometria;

import java.util.List;

import com.itau.geometria.formas.Circulo;
import com.itau.geometria.formas.Retangulo;
import com.itau.geometria.formas.Triangulo;

public class InterfaceSaida {

	public static void imprime(List<Integer> listaTamanhos) {
		switch (listaTamanhos.size()) {
		case 1:
			Circulo circulo = new Circulo(listaTamanhos.get(0));
			break;
		case 2:
			Retangulo retangulo = new Retangulo(listaTamanhos.get(0), listaTamanhos.get(1));
			break;
		case 3:
			try {
				Triangulo triangulo = new Triangulo(listaTamanhos.get(0), listaTamanhos.get(1), listaTamanhos.get(2));
				break;
			} catch (Exception e) {
				break;
			}
		default:
			System.out.println("quantidade de tamanhos nao formam figura desenhada");
			break;
		}
	}
}
