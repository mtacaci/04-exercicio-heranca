package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;

public class App {
	public static void main(String[] args) {
		List<Integer> listaTamanhos = new ArrayList<>();
		listaTamanhos = new InterfaceEntrada().capturaTamanho();
		InterfaceSaida.imprime(listaTamanhos);
	}
}
