package com.itau.geometria.formas;

public class Triangulo extends Forma {
	int ladoA, ladoB, ladoC;

	@Override
	public double calcularArea() {
		double s = (ladoA + ladoB + ladoC) / 2;
		return Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
	}

	public Triangulo(int ladoA, int ladoB, int ladoC) throws Exception {
			if ((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA == true) {
				this.ladoA = ladoA;
				this.ladoB = ladoB;
				this.ladoC = ladoC;
				System.out.println("tamanho triangulo: " + calcularArea());
			} else {
				System.out.println("Lados nao geram um triangulo");
				throw new Exception("Lados nao geram um triangulo");
			}
	}

	public int getLadoA() {
		return ladoA;
	}

	public int getLadoB() {
		return ladoB;
	}

	public int getLadoC() {
		return ladoC;
	}

}
