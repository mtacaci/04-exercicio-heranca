package com.itau.geometria.formas;

public class Circulo extends Forma {
	public int raio;

	@Override
	public double calcularArea() {
		return Math.pow(raio, 2) * Math.PI;
	}

	public  Circulo(int raio) {
		this.raio = raio;
		System.out.println("tamanho circulo: " + calcularArea());
	}

	public int getRaio() {
		return raio;
	}
}
