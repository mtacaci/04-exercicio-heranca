package com.itau.geometria.formas;

public class Retangulo extends Forma {
	int altura, largura;

	@Override
	public double calcularArea() {
		return altura * largura;
	}

	public  Retangulo(int altura, int largura) {
		this.altura = altura;
		this.largura = largura;
		System.out.println("tamanho retangulo: " + calcularArea());
	}

}
