package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InterfaceEntrada {
	
	public int lerInterface(Scanner objetoScanner) {
    	int intTamanho;
    	try {
    	   intTamanho = objetoScanner.nextInt();
    	} catch (Exception E) {
    		System.out.println("tamanho não numérico, continuando com os numeros até o momento");
    		intTamanho=0;
     }
    	return intTamanho;
	}

    public List<Integer>  capturaTamanho() {
    	List<Integer> listaTamanho = new ArrayList<>();
    	Scanner objetoScanner = new Scanner(System.in);
    	int tamanho=0;
    	do {
    		System.out.println("Digite um numero (0 para terminar): ");
    		tamanho=lerInterface(objetoScanner);
    		if (tamanho > 0) {
    		listaTamanho.add(tamanho);
    		}
    	}while (tamanho != 0);
    	return listaTamanho;
    }

}
